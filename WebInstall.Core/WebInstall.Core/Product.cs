﻿using System;

namespace WebInstall.Core
{
    public class Product
    {
        public string Name { get; set; }
        public Uri PackageUrl { get; set; }
        public Uri IconUrl { get; set; }
    }
}