﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace WebInstall.Core
{
    public class Repository
    {
        public List<Product> Products { get; set; } = new List<Product>();
        public string Name { get; set; }
        public Version Version { get; set; }

        public static Repository Resolve(string uri)
        {
            var doc = new XmlDocument();
            var res = new Repository();
            var wc = new WebClient();

            doc.LoadXml(wc.DownloadString(uri));

            if (doc.DocumentElement.Name == "repository")
            {
                foreach (XmlNode childNode in doc.DocumentElement.ChildNodes)
                {
                    if (childNode.Name == "meta")
                    {
                        res.Name = childNode.SelectSingleNode("Name").InnerText;
                        res.Version = new Version(childNode.SelectSingleNode("Version").InnerText);
                    }
                    else
                    {
                        var products = childNode.SelectNodes("product");
                        foreach (XmlNode product in products)
                        {
                            var p = new Product();
                            p.Name = product.SelectSingleNode("Name").InnerText;
                            p.IconUrl = new Uri(product.SelectSingleNode("IconUri").InnerText);
                            p.PackageUrl = new Uri(product.SelectSingleNode("PackageUri").InnerText);

                            res.Products.Add(p);
                        }
                    }
                }
            }

            return res;
        }

        public override string ToString()
        {
            var doc = new XmlDocument();
            var ms = new MemoryStream();
            var writer = new XmlTextWriter(ms, Encoding.ASCII);

            var root  = doc.CreateElement("repository");
            var meta = doc.CreateElement("meta");

            var n = doc.CreateElement("name");
            n.InnerText = Name;
            var v = doc.CreateElement("version");
            v.InnerText = Version.ToString();

            foreach (var product in Products)
            {
                var p = doc.CreateElement("product");
                var nam = doc.CreateElement("Name");
                var ico = doc.CreateElement("IconUri");
                var pack = doc.CreateElement("PackageUri");

                nam.InnerText = product.Name;
                ico.InnerText = product.IconUrl.ToString();
                pack.InnerText = product.PackageUrl.ToString();

                p.AppendChild(nam);
                p.AppendChild(ico);
                p.AppendChild(pack);

                root.AppendChild(p);
            }

            meta.AppendChild(n);
            meta.AppendChild(v);

            root.AppendChild(meta);
            doc.AppendChild(root);

            doc.WriteTo(writer);

            return Encoding.ASCII.GetString(ms.ToArray());
        }
    }
}