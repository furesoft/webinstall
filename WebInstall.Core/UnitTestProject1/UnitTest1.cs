﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebInstall.Core;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Repository_Test()
        {
            var r = new Repository();
            r.Name = "OpenIDE";
            r.Version = new Version(1,0,0,0);

            var p = new Product();
            p.Name = "Shell";
            p.IconUrl = new Uri("http://google.com/");
            p.PackageUrl = new Uri("http://google.com/");

            r.Products.Add(p);

            var xml = r.ToString();
        }
    }
}